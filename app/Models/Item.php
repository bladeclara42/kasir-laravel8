<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\KategoriItem as kategori;


class Item extends Model
{
    protected $guarded = [];

    public function kategori(){
        return $this->belongsTo(kategori::class,'kategori_item_id');
        //di gunakan d tabel penyambung
    }

    public function keranjang(){
        return $this->hasOne(Keranjang::class);

        //hasOne&hasMany d gunakan di tabel induk
        //hasOne iku one to one
        //hasMany one to many
        //belongTo relasai inverse one to many
    }

    public function transaksi(){
        return $this->hasManyThrough(Transaksi::class,DetailTransaksi::class);
    }

}
