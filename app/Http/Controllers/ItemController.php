<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\Item;
use App\Models\KategoriItem;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        view()->share([
            'items' => $items
        ]);

        return view('item');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rasa = KategoriItem::all();

        view()->share([
            'rasa' => $rasa
        ]);
        return view('additem');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Item();
        $file = $request->file('image');

        $name = rand(999999,1);
        $extension = $file->getClientOriginalExtension();
        $newName = $name.'.'.$extension;
        $imgDB = 'uploads/'.$newName;

        $request->image->move(public_path('uploads'), $newName);

        $data->kategori_item_id = $request->rasa;
        $data->gambar = $imgDB;
        $data->persediaan = $request->persediaan;
        $data->nama = $request->name;
        $data->harga = $request->harga;

        $data->save();

        return redirect()->route('items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Item::find($id);
        $rasa = KategoriItem::all();

        view()->share([
            'data' => $data,
            'rasa' => $rasa
        ]);
        return view('edititem');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Item::find($id);
        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads'), $newName);


            $data->gambar = $imgDB;
        }else{
            $data->gambar = $request->newImage;
        }

        $data->kategori_item_id = $request->rasa;
        $data->persediaan = $request->persediaan;
        $data->nama = $request->name;
        $data->harga = $request->harga;

        $data->save();

        return redirect()->route('items.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Item::find($id);

        $data->delete();
        return redirect()->back();
    }
}
