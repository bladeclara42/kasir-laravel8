@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5>Item</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <form class="col s12" action="{{route('items.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="name" name="name" type="text" class="validate">
                                <label for="name">Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <label>Rasa</label>
                            <select class="browser-default" name="rasa">
                                @foreach($rasa as $a)
                                    <option value="{{$a->id}}">{{$a->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="harga" type="number" class="validate">
                                <label for="disabled">Harga</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="persediaan" type="number" class="validate">
                                <label for="disabled">Persediaan</label>
                            </div>
                        </div>
                        <div class="row">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Img</span>
                                        <input type="file" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                        </div>
                        <button class="waves-effect waves-light btn" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

