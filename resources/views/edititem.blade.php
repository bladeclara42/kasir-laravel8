@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5>Item</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <form class="col s12" action="{{route('items.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="name" name="name" value="{{$data->nama}}" type="text" class="validate">
                                <label for="name">Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <label>Rasa</label>
                            <select class="browser-default" name="rasa">
                                <option value="{{$data->kategori_item_id}}">{{$data->kategori->nama}}</option>

                            @foreach($rasa as $a)
                                    <option value="{{$a->id}}">{{$a->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="harga" value="{{$data->harga}}"  type="number" class="validate">
                                <label for="disabled">Harga</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="persediaan" value="{{$data->persediaan}}" type="number" class="validate">
                                <label for="disabled">Persediaan</label>
                            </div>
                        </div>
                        <div class="row">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Img</span>
                                        <input type="file" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                        </div>
                        <input name="newImage" value="{{($data != null) ? $data->gambar : '' }}" hidden>
                        <button class="waves-effect waves-light btn" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

