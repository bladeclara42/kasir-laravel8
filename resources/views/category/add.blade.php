@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h5>Item</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <form class="col s12" action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nama" name="nama" type="text" class="validate">
                            <label for="name">Name</label>
                        </div>
                    </div>
                    <button class="waves-effect waves-light btn" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

