@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h5>Item</h5>
                <a href="{{route('categories.create')}}" class="waves-effect waves-light btn text-white ">Tambah</a>
            </div>
            <div class="card-body">
                <table >
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Roti</th>
                        <th scope="col">action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <th scope="col">{{$loop->iteration}}</th>
                            <td>{{$item->nama}}</td>
                            <td><a href="{{route('categories.edit',$item->id)}}" class="waves-effect waves-light btn text-white blue lighten-1">Edit</a>
                                <a href="{{route('categories.destroy',$item->id)}}" class="waves-effect waves-light btn red text-white">Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
